
#ifndef FLASH_H
#define FLASH_H

#include <Arduino.h>
#include <avr/pgmspace.h>

uint8_t is_valid(uint8_t c);
uint8_t is_number(uint8_t c);
uint8_t is_letter(uint8_t c);
uint8_t is_l_letter(uint8_t c);
uint8_t is_u_letter(uint8_t c);
void ascii_test(int c);

#endif
