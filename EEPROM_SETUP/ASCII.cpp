
#include "ASCII.h"
extern uint8_t serial_in;

char* ascii_array[95] = {" ", "!", "\"", "#", "$", "%", "&", "'", "(", ")",
					   "*", "+", ",", "-", ".", "/", "0", "1", "2", "3",
					   "4", "5", "6", "7", "8", "9", ":", "; ", "<", "=", 
					   ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", 
					   "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", 
					   "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", 
					   "\\", "]", "^", "_", "`", "a", "b", "c", "d", "e", 
					   "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", 
					   "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", 
					   "z", "{", "|", "}", "~"};

uint8_t is_valid(uint8_t c){
	if(c >= 32 && c <= 126) return 1;
	else return 0;
}

uint8_t is_number(uint8_t c){
	if(c >= 48 && c <= 57) return 1;
	else return 0;
}

uint8_t is_letter(uint8_t c){
	if((c >= 65 && c <= 90) || (c >= 97 && c <= 122)) return 1;
	else return 0;
}

uint8_t is_l_letter(uint8_t c){
	if(c >= 97 && c <= 122) return 1;
	else return 0;
}

uint8_t is_u_letter(uint8_t c){
	if(c >= 65 && c <= 90) return 1;
	else return 0;
}

void ascii_test(int input){
	Serial.print(F("input: "));
	Serial.print(serial_in);
	if(is_valid(serial_in) == 1){
		Serial.print(F("   ascii: "));
		Serial.print(ascii_array[serial_in-32]);
	}
	Serial.print(F("\nvalid: "));
	Serial.println(is_valid(serial_in));
	Serial.print(F("number: "));
	Serial.println(is_number(serial_in));
	Serial.print(F("letter: "));
	Serial.println(is_letter(serial_in));
	Serial.print(F("lower: "));
	Serial.println(is_l_letter(serial_in));
	Serial.print(F("upper: "));
	Serial.println(is_u_letter(serial_in));
	Serial.print(F("\n"));
	delay(700);
}