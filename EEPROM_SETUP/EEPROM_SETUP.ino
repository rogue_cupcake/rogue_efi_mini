/*
This is designed to setup the EEPROM of the Rogue EFI mini 
for use. It sets serial number, production date and 
pertaining information.

Also can set base tables although this feature will come later 
and for now is within the actual code for the EFI mini.

1015: lock bit
1016-1023: serial number
1007-1014: date
999-1006: version
991-998: model

*/

#include "Arduino.h"
#include <stdint.h>
#include <EEPROM.h>
#include <avr/pgmspace.h>
#include "ASCII.h"
#include "FUNCTS.h"

#define red_led 11

uint8_t serial_in;
bool led_state = 0x0;
int temp_array[8] = {0,0,0,0,0,0,0,0};
extern char* ascii_array[];

int main(void){
	init();
	pinMode(13,0x1);
	pinMode(red_led,0x1);
	digitalWrite(13, 0x1);
	digitalWrite(red_led, 0x0);
	Serial.begin(2000000);
	Serial.println(F("####################################\n"
					 "    ____  ____  ________  ________\n"
                     "   / __ \\/ __ \\/ ____/ / / / ____/\n"
                     "  / /_/ / / / / / __/ / / / __/\n"
                     " / _, _/ /_/ / /_/ / /_/ / /___\n"
                     "/_/ |_|\\____/\\____/\\____/_____/\n"
                     "             EFI mini\n"
                     "       EEPROM setup program\n\n"
                     "       ©2018 Ginger Pollard\n"
                     "####################################\n"
                     "    Press enter to begin setup!\n"));
	wait_for_enter:
	serial_in = Serial.read();
	if(serial_in != 10) goto wait_for_enter;
	if(EEPROM.read(1015) == 73){ //check for lock bit  //set a good int later 73 for testing!
		Serial.print(F("         LOCK BIT IS SET\n"
			           "UNIT CAN NOT BE PROGRAMED AGAIN!!!!!"));
		LOCKED:
		digitalWrite(red_led,led_state);
		digitalWrite(13, !led_state);
		delay(150);
		led_state = !led_state;
		goto LOCKED;
	}
for(;;){
	digitalWrite(13,0x0);
	Serial.print(F("\n\n\nYou will now be prompted to enter the \n"
				   "setup data. Please follow the onscreen \n"
				   " prompt to setup this Rogue EFI mini!\n"
				   "      Press enter to continue!"));
	wait_for_enter2:
	serial_in = Serial.read();
	if(serial_in != 10) goto wait_for_enter2;
	serial_number:
	Serial.print(F("\n\n\n   Please enter the unit serial number.\n"
				   "*this is the 8 digit number including zeros*\n"
				   "                "));
	int digit = 0;
	enter_serial_number:
	serial_in = Serial.read();
	if(is_number(serial_in) == 1) {
		Serial.print(ascii_array[serial_in-32]);
		temp_array[digit] = serial_in;
		++digit;
		if(digit < 8) goto enter_serial_number;
		Serial.print(F("\n          Press \"Y\" to confirm."));
	}
	else goto enter_serial_number;
	wait_serial_confirm:
	serial_in = Serial.read();
	if(serial_in != 89 && serial_in != 121){
		if(serial_in != 78 && serial_in != 110){
			goto wait_serial_confirm;
		}
		goto serial_number;
	}
	Serial.print(F("\n"));
	for(int x = 0; x <= 7; x++){
		EEPROM.write(1016 + x, temp_array[x]); //1016-1023 are serial number
	}
	digitalWrite(13,0x1);
	delay(500);
	digitalWrite(13,0x0);
	date:
	Serial.print(F("\n\n\n    Please enter the manufacter date.\n"
				   "         *format  mm  dd  yyyy*\n"
				   "               "));
	digit = 0;
	enter_date:
	serial_in = Serial.read();
	if(is_number(serial_in) == 1) {
		Serial.print(ascii_array[serial_in-32]);
		temp_array[digit] = serial_in;
		++digit;
		if(digit == 2 || digit == 4)Serial.print(F("/"));
		if(digit < 8) goto enter_date;
		Serial.print(F("\n          Press \"Y\" to confirm."));
	}
	else goto enter_date;
	wait_date_confirm:
	serial_in = Serial.read();
	if(serial_in != 89 && serial_in != 121){
		if(serial_in != 78 && serial_in != 110){
			goto wait_date_confirm;
		}
		goto date;
	}
	Serial.print(F("\n"));
	for(int x = 0; x <= 7; x++){
		EEPROM.write(1007 + x, temp_array[x]); //1007-1014 are date
	}
	digitalWrite(13,0x1);
	delay(500);
	digitalWrite(13,0x0);
	version:
	Serial.print(F("\n\n\n    Please enter the version number.\n"
				   " *format  main-sub-inneration-variant*\n"
				   "     *if no variant enter a space*\n"
				   "              "));
	digit = 0;
	enter_version:
	serial_in = Serial.read();
	if(is_valid(serial_in) == 1) {
		Serial.print(ascii_array[serial_in-32]);
		temp_array[digit] = serial_in;
		++digit;
		if(digit == 2 || digit == 4)Serial.print(F("."));
		if(digit == 6)Serial.print(" ");
		if(digit < 8) goto enter_version;
		Serial.print(F("\n         Press \"Y\" to confirm."));
	}
	else goto enter_version;
	wait_version_confirm:
	serial_in = Serial.read();
	if(serial_in != 89 && serial_in != 121){
		if(serial_in != 78 && serial_in != 110){
			goto wait_version_confirm;
		}
		goto version;
	}
	Serial.print(F("\n"));
	for(int x = 0; x <= 7; x++){
		EEPROM.write(999 + x, temp_array[x]); //999-1006 are version
	}
	digitalWrite(13,0x1);
	delay(500);
	digitalWrite(13,0x0);
	Serial.print(F("\nOK"));
	model:
	Serial.print(F("\n\n\n  Please enter the model designation.\n"
				   "  *must use 8 digits or characters!*\n"
				   "*use spaces if needed to fill 8 slots*\n"
				   "              "));
	digit = 0;
	enter_model:
	serial_in = Serial.read();
	if(is_valid(serial_in) == 1) {
		Serial.print(ascii_array[serial_in-32]);
		temp_array[digit] = serial_in;
		++digit;
		if(digit < 8) goto enter_model;
		Serial.print(F("\n         Press \"Y\" to confirm."));
	}
	else goto enter_model;
	wait_model_confirm:
	serial_in = Serial.read();
	if(serial_in != 89 && serial_in != 121){
		if(serial_in != 78 && serial_in != 110){
			goto wait_model_confirm;
		}
		goto model;
	}
	Serial.print(F("\n"));
	for(int x = 0; x <= 7; x++){
		EEPROM.write(998 + x, temp_array[x]); //991-998 are model
	}
	digitalWrite(13,0x1);
	delay(500);
	digitalWrite(13,0x0);

	serial_in = Serial.read();
	if((serial_in != 0) && (serial_in != 255 && serial_in != 10)) {
		//ascii_test(serial_in);
	}
	EEPROM.write(1015,72); //lock unit!
	Serial.print("              Finished!\n"__DATE__" "__TIME__"");
	end:

	delay(30);
	led_state = !led_state;
	digitalWrite(13, led_state);
	if(serialEventRun) serialEventRun();
	goto end;
   }
}