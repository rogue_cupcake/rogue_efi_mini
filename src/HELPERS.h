
#ifndef RPM_H
#define RPM_H
#include <Arduino.h>
#include <EEPROM.h>
#include <avr/pgmspace.h>

void DW(int p, boolean s);
void PM(int p, boolean m);
void AR(int p);
void AW(int p, int v);
void E_R(long a);
void logo_print();
void info_print();
void info2_print();
void light_check();
//void delay(long d);

#endif