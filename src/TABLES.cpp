/*
Tables go here!
 */

int VE_table[10][11] = {         //---tune later---
//rpm==0.5k==1k==1.5k==2k==2.5k==3k==3.5k==4k==5k==6k=====tps%
  {5,  14,  21,  26,  30,  32,  33,  34,  35,  36,  37}, //100%
  {5,  14,  20,  24,  28,  30,  30,  31,  33,  36,  36}, //90%
  {5,  13,  19,  23,  26,  27,  28,  28,  31,  34,  35}, //80%
  {5,  12,  18,  21,  24,  25,  25,  25,  28,  31,  33}, //70%
  {5,  12,  16,  19,  21,  22,  23,  23,  25,  29,  31}, //60%
  {5,  11,  15,  17,  19,  19,  20,  20,  22,  26,  29}, //50%
  {5,  11,  13,  15,  17,  17,  18,  19,  20,  24,  26}, //40%
  {5,  10,  11,  13,  15,  15,  16,  17,  18,  22,  24}, //30%
  {5,   9,   9,  11,  12,  13,  14,  15,  16,  20,  22}, //20%
  {5,   8,   8,   9,  10,  11,  12,  13,  15,  18,  21}, //10%
};
