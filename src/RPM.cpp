/*
RPM related shit code
 */

#include "RPM.h"

extern int rpm_raw;
extern float time_last;
extern int rpm_array[5];
extern int rpm;
volatile int rev;
extern long  currentTime_rpm;
extern long looptime_rpm;

void rpms(){
  //detachInterrupt(0);
  cli();
  rpm_raw = 60000/(millis() - time_last)*rev;
  time_last = millis();
  rev = 0;
  //attachInterrupt(0, rpm_interrupt, FALLING);
  sei();
}

void RPM_funct(){
  currentTime_rpm = millis();
  if(currentTime_rpm >= looptime_rpm){
    rpms();
    looptime_rpm = currentTime_rpm;
    //5 Sample Average
    rpm_array[0] = rpm_array[1];
    rpm_array[1] = rpm_array[2];
    rpm_array[2] = rpm_array[3];
    rpm_array[3] = rpm_array[4];
    rpm_array[4] = rpm_raw;
    rpm = (rpm_array[0] + rpm_array[1] + rpm_array[2] + rpm_array[3] + rpm_array[4]) / 5;
  }
}

