/*
Stuff to help out and be lazy!
*/

#include "HELPERS.h"

#define l1 "    ____  ____  ________  ________"
#define l2 "   / __ \\/ __ \\/ ____/ / / / ____/"
#define l3 "  / /_/ / / / / / __/ / / / __/"
#define l4 " / _, _/ /_/ / /_/ / /_/ / /___"
#define l5 "/_/ |_|\\____/\\____/\\____/_____/"

#define LOAD_LOGO //commented out, too much space :(

void logo_print(){
  #ifdef LOAD_LOGO
    Serial.println(F(l1));
    Serial.println(F(l2));
    Serial.println(F(l3));
    Serial.println(F(l4));
    Serial.println(F(l5));
  #endif
}

extern int blue_led;
extern int green_led;
extern int red_led;
extern int CEL;
const char* ver = "0.2.0";
const char* model = "test";
const char* extras = "";
const char* year = "2018";


// because i'm too lazy to type it out
void DW(int p, boolean s){digitalWrite(p,s);}
void PM(int p, boolean m){pinMode(p,m);}
void AR(int p){analogRead(p);}
void AW(int p, int v){analogWrite(p,v);}
void E_R(long a){EEPROM.read(a);}

//Print info
void info_print(){
  Serial.println(F(""));
  Serial.print(F("EFI Ver: "));
  Serial.print(ver);
  Serial.print(F(" , Model: "));
  Serial.println(model);
  int byte0 = EEPROM.read(1022); // !!!!!!!!!!!!fix for new serial number storage!!!!!!!!!!!!!!
  int byte1 = EEPROM.read(1023);  //Serial.print(ascii_array[EEPROM.read(1016 + x) - 32]); need to add ascii.h
  
  Serial.print(F("serial number: "));
  if(byte0<100) Serial.print(F("0"));
  if(byte0<10) Serial.print(F("0"));
  Serial.print(byte0);
  if(byte1<100) Serial.print(F("0"));
  if(byte1<10) Serial.print(F("0"));
  Serial.print(byte1);
  Serial.println("\n");
}
void info2_print(){
  Serial.print(F("©"));
  Serial.print(year);
  Serial.println(F(" Ginger Pollard\n"));
  if(extras != ""){
    Serial.println(extras);
  }
  Serial.print(F("firmware loaded, \nbooting up"));
  for(int x = 0;x <= 8;x++){
     delay(100);
     Serial.print(".");
  }
  delay(100);
  Serial.println(".");
}

void light_check(){
  DW(red_led,0x1);
  DW(green_led,0x1);
  DW(blue_led,0x1);
  DW(CEL,0x1);
  delay(1000);
  DW(red_led,0x0);
  DW(green_led,0x0);
  DW(blue_led,0x0);
  DW(CEL,0x0);
}
