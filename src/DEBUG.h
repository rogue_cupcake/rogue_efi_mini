
#ifndef DEBUG_H
#define DEBUG_H
#include <Arduino.h>
#include <EEPROM.h>
#include <HardwareSerial.h>
#include <avr/pgmspace.h>

int add(int x, int y);

void debug_mode();
void DW(int p, boolean s);
void info_print();

#endif
