/*
Debug shit and stuffy stuff
 */

#include "DEBUG.h"

#define DEBUG_ENABLE //comment out to remove debug mode!

extern bool cel_state;
extern long cel_timer;
extern int CEL;
extern int rpm_raw;
extern float time_last;
extern int rpm_array[5];
extern int rpm;
extern volatile  int rev;
extern long  currentTime_rpm;
extern long looptime_rpm;
extern int VE_table[10][11];
extern int serial_in;

void debug_mode(){
  #ifdef DEBUG_ENABLE
  bool options_shown = 0x0;
  DW(CEL,0x1);
  cel_state = 0x1;
  cel_timer = millis()/1000;
  debug:
  if(!options_shown){
    Serial.println(F("\nYou are in debug mode!"
    "\n\nOptions:\n \"E\"  dump EEPROM\n \"I\"  Unit Info\n \"R\"  dump RPM data\n \"T\"  dump tables\n \"X\"  exit debug\n"));
    options_shown = 0x1;
  }
  debug2:
  if((millis()/1000) >= (cel_timer + 2)){
    cel_state = !cel_state; 
    DW(CEL, cel_state); 
    cel_timer = millis()/1000;
  }
  if(Serial.available() == 0) goto debug2;
  serial_in = Serial.read();
  if(serial_in == 88){
    goto debug_end;
  }
  if(serial_in == 73){
    Serial.println(F("Unit information:"));
    info_print();
    options_shown = 0x0;
  }
  if(serial_in == 69){ 
    Serial.println(F("Dumping EEPROM"));
    for(int x = 0;x <= 8;x++){
      delay(100);
      Serial.print(F("."));
    } 
    delay(100);
    Serial.println(F(".\n"));
    int x4 = 0;
    int val = 0;
    for(long x = 0;x <= 1023;x++){
      val = EEPROM.read(x);
      Serial.print(F("addr: "));
      if(x<1000) Serial.print('0');
      if(x<100) Serial.print('0');
      if(x<10) Serial.print('0');
      Serial.print(x);
      Serial.print(F(" -> "));
      if(val<100) Serial.print('0');
      if(val<10) Serial.print('0');
      Serial.print(val);
      if(x4 >= 3){
        x4 = 0;
        Serial.print(F("\n"));
      }
      else{
        x4++;
        Serial.print(F(" , "));
      }
    }
    Serial.println(F("EEPROM dump complete!\n"));
    options_shown = 0x0;
  }
  if(serial_in == 84){
    Serial.println(F("Dumping Tables"));
    for(int x = 0;x <= 8;x++){
      delay(100);
      Serial.print(F("."));
    } 
    delay(100);
    Serial.print(F(".\n\nVE table:\n"));
    Serial.print(F("=0====0.5k====1k====1.5k====2k====2.5k====3k====3.5k====4k=====5k=====6k========\n"));
    for(int x = 0;x <= 9;x++){
      for(int y = 0; y <= 10;y++){
        if(y == 10){
          if(VE_table[x][y]<10) Serial.print('0');
          Serial.print(VE_table[x][y]);
          Serial.print(F("  |"));
          if(x>0) Serial.print(F(" "));
          Serial.print(10-x);
          Serial.println(F("0 %"));
        }
        else{
          if(VE_table[x][y]<10) Serial.print('0');
          Serial.print(VE_table[x][y]);
          Serial.print(F("  ,  "));
        }
      }
    }
    Serial.print(F("================================================================================\n"));
    Serial.println(F("Table dump complete!\n"));
    options_shown = 0x0;
  }
  if(serial_in == 82){ 
    Serial.println(F("Dumping RPM data"));
    for(int x = 0;x <= 8;x++){
      delay(100);
      Serial.print(F("."));
    } 
    delay(100);
    Serial.println(F(".\n"));
    for(long x = 0;x <= 4;x++){
      Serial.print(F("rpm_array["));
      Serial.print(x);
      Serial.print(F("] = "));
      Serial.println(rpm_array[x]);
    }
    Serial.print(F("rpm_raw = "));
    Serial.print(rpm_raw);
    Serial.print(F("  currentTime_rpm = "));
    Serial.print(currentTime_rpm);
    Serial.print(F("\ntime_last = "));
    Serial.print(time_last);
    Serial.print(F("  looptime_rpm = "));
    Serial.print(looptime_rpm);
    Serial.print(F("  rev = "));
    Serial.print(rev);
    Serial.println(F("\nRPM data dump complete!\n"));
    options_shown = 0x0;
  }
  goto debug;

  debug_end:
  Serial.println(F("Exiting debug mode"));
  for(int x = 0;x <= 8;x++){
    delay(100);
    Serial.print(F("."));
  } 
  delay(100);
  Serial.println(F(".\n"));
  options_shown = 0x0;
  #endif
}
