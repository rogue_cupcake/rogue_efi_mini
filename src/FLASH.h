
#ifndef FLASH_H
#define FLASH_H
#include <Arduino.h>
#include <HardwareSerial.h>
#include <avr/pgmspace.h>

void flash_mode();
void debug_mode();
void DW(int p, boolean s);

#endif
