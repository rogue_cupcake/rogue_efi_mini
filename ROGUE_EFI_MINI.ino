/*
Rogue EFI mini v 0.2.0 and up
does not support version ZERO
IAC and fan pins have to be flipped only for v0.2.0!!!!!

TODO: switch to .h and .cpp to be more proper lol
*/

#include "Arduino.h"
#include <stdint.h>
#include <EEPROM.h>
#include <avr/pgmspace.h>

int FPUMP = A0;
int TRIM_in = A1;
int ECT_in = A2;
int IAT_in = A3;
int MAP_in = A4;
int TPS_in = A5;
int RX = 0;
int TX = 1;
int RPM_in = 2;
int blue_led = 3;
int green_led = 4;
int red_led = 5;
int IAC = 6;
int FAN = 7;
int ECR = 8;
int INJ_1 = 11;
int INJ_2 = 12;
int CEL = 13;

extern void flash_mode();


#include "src/TABLES.h"
#include "src/RPM.h"
#include "src/DEBUG.h"
#include "src/FLASH.h"

bool over_rev = 0x0;
int rev_limit = 65; // *100 in function
bool cel_state = 0x0;
long cel_timer;
extern uint8_t serial_in;
int8_t rpm_raw = 0;
float time_last = 0;
int8_t rpm_array[5] = {0,0,0,0,0};
int8_t rpm = 0;
long  currentTime_rpm = 0;
long looptime_rpm = 0;
extern volatile int rev;
const bool trim_type = 0x0; // 0x0: trim pot , 0x1: O2 sensor
extern void logo_print();
extern void info_print();
extern void info2_print();
extern void light_check();
extern void DW(int p, boolean s);
extern void PM(int p, boolean m);
extern void AR(int p);
extern void AW(int p, int v);
extern void E_R(long a);
extern void RPM_funct();
void rpm_interrupt()
{
  cli();
  rev++;
}

int main(void){
  init();
  sw_reset: //a software reset point
  attachInterrupt(0, rpm_interrupt, FALLING);
  sei();
  init();
  Serial.begin(2000000);
  logo_print();
  info_print();
  info2_print();
  PM(CEL,0x1);
  PM(red_led, 0x1);
  PM(green_led, 0x1);
  PM(blue_led, 0x1);
  light_check();

   
for(;;){
  flash_mode();
  
  start:

  if(rpm >= (rev_limit*100)){
    over_rev = 0x1;
  }
  else{
    over_rev = 0x0;
  }

  











 

  RPM_funct();

  if(serialEventRun) serialEventRun();

   }
}
